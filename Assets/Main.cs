﻿using System.Diagnostics;
using System.Text;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{

	[SerializeField] private Text text;

	private  static long item;
	
	// Use this for initialization
	void Start ()
	{
		var benchmarkResult = RunBenchmark();
		text.text = benchmarkResult;
	}

	static unsafe string RunBenchmark()
	{
		const int ITERATIONS = 100000000;
		
		var sb = new StringBuilder();
		sb.AppendLine("Incorrect layout " + sizeof(StructWithIncorrectLayout));
		sb.AppendLine("Auto layout " + sizeof(StructWithAutoLayout));
		sb.AppendLine("Correct layout " + sizeof(StructWithCorrectLayout));
		sb.AppendLine("Explicit layout " + sizeof(StructWithExplicitLayout));
		sb.AppendLine("SlowStruct layout " + sizeof(MisalignedStruct));
		sb.AppendLine("FastStruct layout " + sizeof(AlignedStruct));

		var fastStruct = new AlignedStruct();
		var slowStruct = new MisalignedStruct();
		var stopwatch = new Stopwatch();
		stopwatch.Start();
		for (var i = 0; i < ITERATIONS; ++i)
		{
			item = fastStruct.x + fastStruct.y;
		}

		sb.AppendLine("Fast struct " + stopwatch.ElapsedMilliseconds);

		stopwatch.Restart();
		for (var i = 0; i < ITERATIONS; ++i)
		{
			item = slowStruct.x + slowStruct.y;
		}

		sb.AppendLine("Slow struct " + stopwatch.ElapsedMilliseconds);
		sb.AppendLine(item.ToString());
		return sb.ToString();
	}
}
