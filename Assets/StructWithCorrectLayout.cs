﻿using System.Runtime.InteropServices;

namespace DefaultNamespace
{
    public struct StructWithCorrectLayout
    {
        public int x;
        public int y;
        public short hitpoints;
        public short attackPower;
    }
    
}