﻿using System.Runtime.InteropServices;

namespace DefaultNamespace
{
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public struct AlignedStruct
    {
        public byte hitpoints;
        public long x;
        public byte attackPower;
        public long y;
    }
}