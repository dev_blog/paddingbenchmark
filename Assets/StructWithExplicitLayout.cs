﻿using System.Runtime.InteropServices;

namespace DefaultNamespace
{
    [StructLayout(LayoutKind.Explicit, Size = 12)]
    public struct StructWithExplicitLayout
    {
       [FieldOffset(0)] public int x;
       [FieldOffset(4)] public short hitpoints;
       [FieldOffset(6)] public int y;
       [FieldOffset(10)] public short attackPower;     
    }
}