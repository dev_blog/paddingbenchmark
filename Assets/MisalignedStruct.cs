﻿using System.Runtime.InteropServices;

namespace DefaultNamespace
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MisalignedStruct
    {
        public byte hitpoints;
        public long x;
        public byte attackPower;
        public long y;    
    }
}