﻿using System.Runtime.InteropServices;

namespace DefaultNamespace
{
    [StructLayout(LayoutKind.Auto, Size = 12, Pack = 0)]
    public struct StructWithAutoLayout
    {
        public short hitpoints;   
        public int x;    
        public short attackPower;
        public int y;
    }
}